package com.micro.fast.upms.dao;

import com.micro.fast.common.dao.SsmMapper;
import com.micro.fast.upms.pojo.UpmsSystem;
import feign.Param;

import java.util.List;

public interface UpmsSystemMapper extends SsmMapper<UpmsSystem,Integer> {

    @Override
    int deleteByPrimaryKey(Integer id);

    @Override
    int insert(UpmsSystem record);

    @Override
    int insertSelective(UpmsSystem record);

    @Override
    UpmsSystem selectByPrimaryKey(Integer id);

    @Override
    int updateByPrimaryKeySelective(UpmsSystem record);

    @Override
    int updateByPrimaryKey(UpmsSystem record);

    @Override
    List<UpmsSystem> selectByCondition(UpmsSystem record);

    @Override
    int deleteByPrimaryKeys(List<Integer> ids);
}